from pymongo import MongoClient
from config import (uri)
from datetime import datetime

import boto3
import csv
import time
import os

client = MongoClient(uri)
db = client.BigData
collection = db.results

infos = []
sqs = boto3.resource("sqs")
s3 = boto3.resource("s3")
response_queue = sqs.get_queue_by_name(QueueName='big-data-response')
bucket = s3.Bucket('big-data-bucket')
jobIdToName = {
    '0': 'pastor', 
    '1': 'model', 
    '2': 'yoga_teacher', 
    '3': 'teacher', 
    '4': 'personal_trainer',
    '5': 'painter',
    '6': 'journalist',
    '7': 'interior_designer',
    '8': 'surgeon',
    '9': 'accountant',
    '10':'dj',
    '11':'physician',
    '12':'comedian',
    '13':'software_engineer',
    '14':'nurse',
    '15':'poet',
    '16':'dentist',
    '17':'chiropractor',
    '18':'filmmaker',
    '19':'professor',
    '20':'photographer',
    '21':'rapper',
    '22':'psychologist',
    '23':'paralegal',
    '24':'architect',
    '25':'composer',
    '26':'attorney',
    '27':'dietitian'}
def wait_for_response(queue):
    print("Waiting for response...")
    output = "?"
    while output == "?":
        time.sleep(1)
        messages = queue.receive_messages(
            MaxNumberOfMessages=1,
            MessageAttributeNames=['All']
        )
        
        for message in messages:
            output = message.body
            message.delete()
            break
        
    return output

#output = wait_for_response(response_queue)
print("Downloading from bucket...")
#bucket.download_file(output, "predict.csv")
print("Reading CSV...")
with open('predict.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter='|')
    for row in spamreader:
        infos.append({ "text": row[0], "job": jobIdToName.get(row[1]) })

now = datetime.now()
d1 = now.strftime("%d/%m-%H_%M")
print("Inserting data in Mongo...")
collection.insert_one({d1: infos})
os.remove('./predict.csv')
