from os import path, getenv
from dotenv import load_dotenv

# Load environment variables from .env
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

uri = getenv('URI')
remote_path = getenv('REMOTE_PATH')
